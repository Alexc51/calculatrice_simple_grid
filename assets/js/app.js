// input text résultat
let textview = document.getElementById('textview');
// regle de regex pour le '.'
const regex = /\s?([.])\s?/;

// Efface
function getClean() {
    textview.value = "";
}

// Retour sur la valeur actuel
function getBack() {
    let results = textview.value;
    textview.value = results.substring(0, results.length - 1);
}

//Insert le num
function getInsert(num) {
    textview.value = textview.value + num;
}

// results sans eval(), resultat global
function getEqual() {
    let results = textview.value;
    verif = results[results.search(regex)];
    if ("." == verif) {
        textview.value = (new Function('return ' + results)().toFixed(2));
    } else {
        textview.value = (new Function('return ' + results)());
    }
}

// équal avec entrer
textview.addEventListener("keyup", function (event) {
    if (event.keyCode === 13) {
        event.preventDefault();
        getEqual();
    }
});